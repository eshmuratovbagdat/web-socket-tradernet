package com.test.tradernettest.ui

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.test.tradernettest.R
import com.test.tradernettest.model.Quote
import com.test.tradernettest.ui.MainListAdapter.MainViewHolder
import com.test.tradernettest.utils.background
import com.test.tradernettest.utils.checkItem
import com.test.tradernettest.utils.textColor
import kotlinx.android.synthetic.main.item_view.view.*

class MainListAdapter : RecyclerView.Adapter<MainViewHolder>() {
    private val items: MutableList<Quote> = mutableListOf()
    private lateinit var context: Context

    inner class MainViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name = v.name
        val procent = v.procent
        val info = v.info
        val companyName = v.companyName

        @SuppressLint("SetTextI18n")
        fun bind(item: Quote) {
            name.text = item.c
            companyName.text = "${item.ltr} | ${item.name}"
            info.text = "${item.ltp} (${item.chg})"
            item.pcp?.let {
                procent.text = String.format(context.getString(R.string.percent), it)
                if (item.changed) {
                    procent.setBackgroundResource(it.background())
                    procent.setTextColor(ContextCompat.getColor(context, R.color.white))
                } else {
                    procent.setTextColor(ContextCompat.getColor(context, it.textColor()))
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainViewHolder {
        context = parent.context
        val layout = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false)
        return MainViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount() = items.size

    fun addItem(item: Quote) {
        val index = items.indexOf(item)
        if (index != -1) {
            items[index].checkItem(item)
            notifyItemChanged(index)
        } else {
            if (item.c == null || item.pcp == null || item.name == null) return
            items.add(item)
            notifyDataSetChanged()
        }
    }
}
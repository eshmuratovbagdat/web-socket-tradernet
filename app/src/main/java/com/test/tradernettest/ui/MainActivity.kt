package com.test.tradernettest.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.tradernettest.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModels()
    private val quoteAdapter by lazy { MainListAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(recyclerView) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = quoteAdapter
        }

        observeViewModel()
        viewModel.connectToServer()
    }

    private fun observeViewModel() {
        viewModel.quotes.observe(this, {
            progressBar.visibility = View.INVISIBLE
            (recyclerView.adapter as MainListAdapter).addItem(it)
        })
    }
}
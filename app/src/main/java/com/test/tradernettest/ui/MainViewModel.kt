package com.test.tradernettest.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.test.tradernettest.model.Quote
import com.test.tradernettest.utils.Configure
import com.test.tradernettest.utils.jsonToObject
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import java.net.URI
import javax.net.ssl.SSLSocketFactory


class MainViewModel : ViewModel() {
    private var webSocketClient: WebSocketClient? = null
    private val gson by lazy { Gson() }
    val error: MutableLiveData<Exception> = MutableLiveData()
    val quotes: MutableLiveData<Quote> = MutableLiveData()

    fun connectToServer() {
        createWebSocketClient()
        val socketFactory: SSLSocketFactory = SSLSocketFactory.getDefault() as SSLSocketFactory
        webSocketClient?.setSocketFactory(socketFactory)
        webSocketClient?.connect()
    }

    private fun createWebSocketClient() {
        webSocketClient = object : WebSocketClient(URI(Configure.URL)) {
            override fun onOpen(handshakedata: ServerHandshake?) {
                webSocketClient?.send(Configure.realtimeQuotes)
            }

            override fun onMessage(message: String?) {
                message?.let {
                    quotes.postValue(it.jsonToObject(gson))
                }
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {}

            override fun onError(ex: Exception?) {
                error.postValue(ex)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        webSocketClient?.close()
    }
}
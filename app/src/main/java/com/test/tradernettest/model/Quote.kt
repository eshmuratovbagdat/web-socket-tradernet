package com.test.tradernettest.model

data class Quote(
    var c: String?,
    var name: String?,
    var pcp: Float?,
    var chg: Float?,
    var ltr: String?,
    var ltp: String?,
    var changed: Boolean = false
) {
    override fun equals(other: Any?): Boolean {
        if (other == null) {
            return false
        }

        if (other::class != this::class) {
            return false
        }

        val o: Quote = other as Quote

        return this.c == o.c
    }

    override fun hashCode(): Int {
        var hash = 3
        hash = 53 * hash + this.c.hashCode()
        return hash
    }
}

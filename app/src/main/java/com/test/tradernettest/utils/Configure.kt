package com.test.tradernettest.utils

object Configure {
    const val realtimeQuotes = "[\"realtimeQuotes\", [\"SBER\", \"NBL.US\", \"RSTI\", \"GAZP\", \"HYDR\", \"VTBR\", \"ANH.US\", \"VICL.US\", \"BURG.US\", \"SGGD.EU\", \"AAPL.SPB\"]]"
    const val URL = "wss://wss.tradernet.ru"
}
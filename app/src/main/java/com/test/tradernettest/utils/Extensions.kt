package com.test.tradernettest.utils

import com.google.gson.Gson
import com.test.tradernettest.R
import com.test.tradernettest.model.Quote
import org.json.JSONObject

fun String.jsonToObject(gson: Gson): Quote {
    val jsonObject = JSONObject(this.substring(this.indexOf("{"), this.lastIndexOf("}") + 1))
    return gson.fromJson(jsonObject.toString(), Quote::class.java)
}

fun Quote.checkItem(item: Quote) {
    item.chg?.let { this.chg = it }
    item.ltr?.let { this.ltr = it }
    item.pcp?.let { this.pcp = it }
    item.ltp?.let { this.ltp = it }
    item.name?.let { this.name = it }
    item.c?.let { this.c = it }
    this.changed = true
}

fun Float.textColor(): Int {
    return if (this >= 0f) R.color.green else R.color.red
}

fun Float.background(): Int {
    return if (this > 0f) R.drawable.green_bg else R.drawable.red_bg
}